package com.delivery.prueba.service;

import com.delivery.prueba.entity.Category;
import com.delivery.prueba.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CategoryService {

    @Autowired
    CategoryRepository categoryRepository;



    public List<Category> listCategory() {
        return categoryRepository.findAll();
    }
}
