INSERT INTO CATEGORY(ID, NAME, ICON) VALUES (1, 'ALL','@/assets/icons/701965.svg');
INSERT INTO CATEGORY(ID, NAME, ICON) VALUES (2, 'Pizza','@/assets/icons/599995.svg');
INSERT INTO CATEGORY(ID, NAME, ICON) VALUES (3, 'Asian','@/assets/icons/1895684.svg');
INSERT INTO CATEGORY(ID, NAME, ICON) VALUES (4, 'Burgers','@/assets/icons/883806.svg');
INSERT INTO CATEGORY(ID, NAME, ICON) VALUES (5, 'Barbecue','@/assets/icons/933310.svg');
INSERT INTO CATEGORY(ID, NAME, ICON) VALUES (6, 'Dessers','@/assets/icons/174394.svg');
INSERT INTO CATEGORY(ID, NAME, ICON) VALUES (7, 'Thai','@/assets/icons/135367.svg');
INSERT INTO CATEGORY(ID, NAME, ICON) VALUES (8, 'Sushi','@/assets/icons/1900683.svg');

INSERT INTO PRODUCTS(id,name,qualification,time,price,image) VALUES (1,'Filete de ternera con salsa',4.9,'25-30min',14.99,'https://images.pexels.com/photos/675951/pexels-photo-675951.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=226&w=440');
INSERT INTO PRODUCTS(id,name,qualification,time,price,image) VALUES (2,'Desayuno de primer plano lácteos',4.7,'20-25min',9.99,'https://images.pexels.com/photos/376464/pexels-photo-376464.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=226&w=440');
INSERT INTO PRODUCTS(id,name,qualification,time,price,image) VALUES (3,'Burrito de pollo',4.6,'25-30min',13.99,'https://images.pexels.com/photos/461198/pexels-photo-461198.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=226&w=440');
INSERT INTO PRODUCTS(id,name,qualification,time,price,image) VALUES (4,'Plato de salmón',4.5,'25-30min',15.99,'https://images.pexels.com/photos/46239/salmon-dish-food-meal-46239.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=226&w=440');
INSERT INTO PRODUCTS(id,name,qualification,time,price,image) VALUES (5,'Postre',4.8,'15-20min',6.99,'https://images.pexels.com/photos/1099680/pexels-photo-1099680.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=226&w=440');
INSERT INTO PRODUCTS(id,name,qualification,time,price,image) VALUES (6,'Sándwich servido en la tabla de cortar',4.7,'10-15min',9.99,'https://images.pexels.com/photos/1600711/pexels-photo-1600711.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=226&w=440');
INSERT INTO PRODUCTS(id,name,qualification,time,price,image) VALUES (7,'Plato de aguacate cocido',4.4,'15-25min',12.99,'https://images.pexels.com/photos/262959/pexels-photo-262959.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=226&w=440');
INSERT INTO PRODUCTS(id,name,qualification,time,price,image) VALUES (8,'Hamburguesa',4.6,'10-15min',14.99,'https://images.pexels.com/photos/156114/pexels-photo-156114.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=226&w=440');






