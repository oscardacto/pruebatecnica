package com.delivery.prueba.controller;

import com.delivery.prueba.entity.Category;
import com.delivery.prueba.entity.Product;
import com.delivery.prueba.service.CategoryService;
import com.delivery.prueba.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/product")
public class ProductController {
    @Autowired
    ProductService productService;
/*
    @PostMapping("/create")
    public String createCategory(@RequestBody Category category) {
        categoryService.createCategory(category);
        return "success";
    }
*/
    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/list")
        public List<Product> listProduct() {
            return productService.listProduct();
        }
}
