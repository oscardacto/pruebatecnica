package com.delivery.prueba.service;

import com.delivery.prueba.entity.Category;
import com.delivery.prueba.entity.Product;
import com.delivery.prueba.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    /*public void createProduct(Product product) {
        productRepository.save(product);
    }*/

    public List<Product> listProduct() {
        return productRepository.findAll();
    }
}
