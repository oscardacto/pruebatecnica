package com.delivery.prueba.controller;

import com.delivery.prueba.entity.Category;
import com.delivery.prueba.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;


    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/list")
    public List<Category> listCategory() {
        return categoryService.listCategory();
    }
}
